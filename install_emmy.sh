#! /bin/bash

# check to see if emmy alias exists
shopt -s expand_aliases
if [[ -f $HOME"/.aliases" ]]; then
    source $HOME"/.aliases"
fi

emmy_installed=$(alias | grep "emmy")
emmy_alias="alias emmy='fortune myfortune | cowsay -f small'"
if [[ $emmy_installed != $emmy_alias ]]
then
    #determine shell
    shell_rc=$HOME"/."$(echo $SHELL | sed 's:.*/::')"rc"
    
    #get determine if emmy alias exists in .aliases file
    if [[ -f $HOME"/.aliases" || $emmy_alias == $(cat $HOME/.aliases | grep -o $emmy_alias) ]]; then
        echo $emmy_alias >> $HOME"/.aliases"
    fi

    #add to shell_rc
    if [[ 'source $HOME/.aliases' != *$(cat $shell_rc | grep -o 'source $HOME/.aliases')* && 'source ~/.aliases' == $(cat $shell_rc | grep -o 'source ~/.aliases') ]]; then
        echo 'source $HOME/.aliases' >> $shell_rc
    fi
else
    echo "Emmy alias ready exists, it is:"
    echo $emmy_installed
    exit 1
fi

echo "Emmy is installed. Run \"souce "$shell_rc"\" to start using emmy."
