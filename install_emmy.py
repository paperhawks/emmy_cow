import os

def read_file_for_string(in_file,string):
    """
    Reads a file and determines whether that string
    lives inside that file.

    Input: 
    in_file - Path to the file to be read.
    string - String that is being searched.

    Output:
    bool - Boolean as to whether that file exists.
    """
    for line in in_file:
        if string in line:
            return True

    return False

def setup_aliases_file(home_dir, cow_type):
    """
    Sets up the aliases file under $HOME/.aliases and adds 
    the emmy cow command.

    Input: 
    cow_type - Character display for cowsay.
    home_dir - Home directory for the user.
    """
     # create the alias file if it doesn't exist
    if not os.path.exists(home_dir+'/.aliases'):
        alias_file = open(home_dir+'/.aliases','a')
        alias_file.write("alias emmy=\'fortune myfortune | cowsay -f "+cow_type+"\'")
        alias_file.close()

    else:
        # check to see if emmy alias exists
        alias_file = open(home_dir+'/.aliases')
        if read_file_for_string(alias_file,"alias emmy=\'fortune myfortune | cowsay -f "+cow_type+"\'"):
            print("Emmy cow exists in aliases.")
            alias_file.close()
            return

        #create alias in ~/.aliases if it does not exist
        else:
            alias_file = open(home_dir+'/.aliases','a')
            print("opened aliases file for writing")
            alias_file.write("alias emmy=\'fortune myfortune | cowsay -f "+cow_type+"\'")
            alias_file.close()
    

def main():
    cow_type = "tux"

    #check which shell to use
    shell = os.environ['SHELL']
    home_dir = os.environ['HOME']

    # check to see if emmy alias already exists.
    if False:#os.system('which emmy') is not "emmy not found":
        print("emmy alias exists.")

    # create emmy cow if it does not
    else:
        setup_aliases_file(home_dir, cow_type)
        
        shell_rc_file_dir = "" 
        # Determine which type of shell  to append
        if "bash" in shell:
            # check to see if bashrc contains an alias file
            shell_rc_file_dir = home_dir+"/.bashrc"
        elif "zsh" in shell:
            shell_rc_file_dir = home_dir+"/.zshrc"
        shell_rc_file = open(shell_rc_file_dir,'r')

        if not read_file_for_string(shell_rc_file,"source ~/.aliases") and not read_file_for_string(shell_rc_file,"source ~/.aliases"):
            shell_rc_file = open(shell_rc_file_dir,'a')
            shell_rc_file.write("source ~/.aliases")
        
if __name__ == "__main__":
    main()
